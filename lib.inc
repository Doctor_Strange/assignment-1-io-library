%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ 0
%define STDOUT 1
%define STDIN 0



section .text
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT		;syscall номер для sys_exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
		xor rax, rax
	.loop:
		mov cl, [rdi]
		test cl, cl
		jz .exit
		inc rdi
		inc rax
		jmp .loop
	.exit:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, SYS_WRITE                                 
	mov rdi, STDOUT
	syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
    mov rax, SYS_WRITE
	mov rsi, rsp
	mov rdi, STDOUT
	mov rdx, 1
	syscall
	pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA 
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
		mov rax, rdi
		sub rsp, 21	 ;Максимальное пространство, необходимое для хранения десятичного представления 64-битного целого числа без знака, включая нулевой терминатор.
		mov byte [rsp + 20], 0
		lea rsi, [rsp + 20]
		mov rcx, 10
	.loop:
		xor rdx, rdx
		div rcx
		add dl, '0'
		dec rsi
		mov [rsi], dl
		test rax, rax
		jnz .loop
	.print:
		mov rdi, rsi
		call print_string		
		add rsp, 21
		ret
		
; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
		test rdi, rdi
		jge .plus
		neg rdi
		push rdi
		mov rdi, '-'
		call print_char
		pop rdi
	.plus:
		jmp print_uint
	
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
		xor rcx, rcx
	.loop:
		mov al, [rdi]
		cmp al, [rsi]
		jne .not_equal
		inc rdi
		inc rsi
		test al, al
		jz .equal
		jmp .loop
	.equal:
		mov eax, 1
		ret
	.not_equal:
		mov eax, 0
		ret
		
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
		mov rax, SYS_READ
		mov rdi, STDIN
		lea rsi, [rsp-1]
		mov rdx, 1	
		syscall	
		cmp rax, 1          
		jne .eof          
		movzx rax, byte [rsp-1] 
		ret	
	.eof:
		xor rax, rax    
		ret
		
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
		push r12
		push r13
		push r14
		mov r12, rdi ;r12 = adress
		mov	r13, rsi ;r13 = buffer
		xor r14, r14 ;string length		
	.find_whitespace:
		call read_char
		cmp al, ` `
		je .find_whitespace
		cmp al, `\t`
		je .find_whitespace
		cmp al, `\n`
		je .find_whitespace
	.next_char:
		cmp al, ` `
		je .return_string
		cmp al, `\t`
		je .return_string
		cmp al, `\n`
		je .return_string
		test rax, rax
		jz .return_string
		cmp r14, r13
		jg .buffer_overflow
		mov byte[r12 + r14], al
		inc r14
		call read_char
		jmp .next_char
	.return_string:
		mov byte[r12 + r14], 0
		mov rax, r12
		mov rdx, r14
		pop r14
		pop r13
		pop r12
		ret
	.buffer_overflow:
		xor rax, rax
		xor rdx, rdx
		pop r14
		pop r13
		pop r12
		ret
		
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
		xor rax, rax          ; Очищаем rax, здесь будет число
		xor rdx, rdx          ; Очищаем rdx, здесь будет длина
	.A:
		movzx r8, byte[rdi] ; Загружаем символ из строки
		test r8b, r8b     ; Проверяем, достигли ли конца строки
		jz .B           ; Если да, завершаем
		sub r8, '0'        ; Вычитаем '0', чтобы получить значение
		cmp r8, 9          ; Проверяем, является ли символ числом от 0 до 9
		ja .B            ; Если нет, завершаем
		; Добавляем текущее число в rax
		imul  rax, rax, 10    ; Умножаем текущее число на 10
		add   rax, r8         ; Добавляем новую цифру
		inc   rdi             ; Переходим к следующему символу
		inc   rdx             ; Увеличиваем длину
		jmp   .A      ; Повторяем для следующего символа
	.B:
		ret
		
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:  
		movzx rcx, byte [rdi]
		cmp rcx, '-'
		jne .plus
		inc rdi           
		call parse_uint 
		test rdx, rdx
		jz .return
		inc rdx
		neg rax
		jmp .return
	.plus:
		cmp rcx, '+'
		jne .no_sign
		inc rdi           
	.no_sign:
		jmp parse_uint   
	.return:         
		ret
		
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
		push rdi
		push rsi
		push rdx
		xor rcx, rcx
		call string_length
		pop rdx
		pop rsi
		pop rdi
		cmp rax, rdx
		jae .buffer_overflow
	.loop:
		mov al, [rdi]
		mov [rsi], al
		inc rdi
		inc rsi
		test al, al
		jz .end
		jmp .loop
	.end:
		mov rax, rcx
		ret
	.buffer_overflow:
		xor rax, rax
		ret
